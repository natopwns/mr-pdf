# tkinter_functions.py - Functions specific to the Tkinter GUI.

# Copyright (C) 2019  Nathan Taylor
# GPL-3.0-or-later

from tkinter.filedialog import askopenfilename
import d_error

# Functions---------------------------------------------------------------------
def tk_pick_file(GUI):

	GUI.outputText.set('Processing...')

	inputFile = askopenfilename(
		title="Select PDF",
		filetypes=[("PDF","*.pdf"),("All files","*.*")]
	)

	if type(inputFile) is tuple:
		GUI.outputText.set('Please choose a PDF file.')
		return False

	if inputFile == '':
		GUI.outputText.set('Please choose a PDF file.')
		return False

	if inputFile[inputFile.rfind('.'):len(inputFile)] != '.pdf':
		GUI.outputText.set('Please choose a PDF file.')
		return False

	return inputFile

def report_callback_exception(self, error_class, error_string, traceback):

	error_dialog = d_error.ErrorDialog(self, error_string)

	self.destroy()

# Functions---------------------------------------------------------------------

