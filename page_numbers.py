# page_numbers.py - Functions related to adding page numbers to PDFs.

# Copyright (C) 2019  Nathan Taylor
# GPL-3.0-or-later

import sys
import math
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from PyPDF2 import PdfWriter, PdfReader

DEFAULT_PAGE_SIZE = [612, 792]

OFFSETS = {
	'box':    [63, 44],
	'number': [20, 26],
	'notes':  [20, 20]
}

# ---------
# FUNCTIONS
# ---------

def load_pdf(input_file):

	input_pdf = PdfReader(input_file)

	return input_pdf

def get_number_of_pages(input_pdf):

	number_of_pages = len(input_pdf.pages)

	return number_of_pages

def get_orientation_list(input_pdf, number_of_pages):

	orientation_list = []

	for page_num in range(number_of_pages):

		page_dimensions = input_pdf.pages[page_num].mediabox
		urX = page_dimensions.right
		ulX = page_dimensions.left
		urY = page_dimensions.top
		lrY = page_dimensions.bottom
		x = float(urX - ulX)
		y = float(urY - lrY)

		if x > y:
			orientation = 'landscape'
		else:
			orientation = 'portrait'

		rotation = input_pdf.pages[page_num].get('/Rotate')
		orientation_list.append([orientation, rotation, x, y])

	return orientation_list

def generate_page_numbers(pages, orientation, position, notes):

	# instantiate a new canvas object
	canvas_numbers = canvas.Canvas('page_numbers.pdf', pagesize=letter)

	for page in range(pages):

		# get page info
		page_orient  = orientation[page][0]
		width        = orientation[page][2]
		height       = orientation[page][3]

		if page_orient == 'landscape':
			default_x    = DEFAULT_PAGE_SIZE[1]
			default_y    = DEFAULT_PAGE_SIZE[0]
			offset_num_x = OFFSETS['number'][1] - 6  # too lazy to figure out what's wrong with my formula
			offset_num_y = OFFSETS['number'][0] + 6
			offset_notes_x = OFFSETS['notes'][1]
			offset_notes_y = OFFSETS['notes'][0]
		else:
			default_x    = DEFAULT_PAGE_SIZE[0]
			default_y    = DEFAULT_PAGE_SIZE[1]
			offset_num_x = OFFSETS['number'][0]
			offset_num_y = OFFSETS['number'][1]
			offset_notes_x = OFFSETS['notes'][0]
			offset_notes_y = OFFSETS['notes'][1]

		# get page scale
		scale_x = width  / default_x
		scale_y = height / default_y

		# generate positions
		if position == 1:
			num_pos_x = width  - (offset_num_x * scale_x)
			num_pos_y = height - (offset_num_y * scale_y)
		elif position == 2:
			num_pos_x = width  - (offset_num_x * scale_x)
			num_pos_y = (offset_num_y - 6) * scale_y  # adjust offset to visually match spacing

		note_pos_x = offset_notes_x * scale_x
		note_pos_y = offset_notes_y * scale_y

		# DEBUG-------------------------------------------------------------------
		print('Page', page + 1, ' x:', width, ' y:', height, ' rot:', page_orient)
		# ------------------------------------------------------------------------

		# setup canvas
		canvas_numbers.setPageSize((width, height))
		canvas_numbers.setFont('Helvetica', 10)
		# draw a square for the page number
		canvas_numbers.setFillGray(1.0)
		canvas_numbers.rect(num_pos_x - 23, num_pos_y - 4, 26, 16, stroke=1, fill=1)
		canvas_numbers.setFillGray(0.0)
		# draw a page number in the top-right corner of the page
		canvas_numbers.drawRightString(num_pos_x, num_pos_y, str(page + 1))
		# draw notes in the bottom left corner (if applicable)
		if notes != '':
			canvas_numbers.drawString(note_pos_x, note_pos_y, notes)
		# save and end the current page (resets rotation)
		canvas_numbers.showPage()

	# save to disk
	canvas_numbers.save()

def overlay_page_numbers(input_pdf, output_file, numbers_file):

	numbers_pdf = PdfReader(numbers_file)
	pdf_writer = PdfWriter()

	for page in range(len(input_pdf.pages)):
		# DEBUG----------------------------
		print('Overlaying page:', page + 1)
		# ---------------------------------
		input_page = input_pdf.pages[page]
		numbers_page = numbers_pdf.pages[page]
		input_page.merge_page(numbers_page)
		pdf_writer.add_page(input_page)

	# save the new PDf to disk
	with open(output_file, 'wb') as output_pdf:
		pdf_writer.write(output_pdf)

def add_page_numbers(input_file, position, notes):

	output_file      = input_file[:input_file.rfind('.')] + '_numbered.pdf'
	input_pdf        = load_pdf(input_file)
	number_of_pages  = get_number_of_pages(input_pdf)
	orientation_list = get_orientation_list(input_pdf, number_of_pages)

	generate_page_numbers(number_of_pages, orientation_list, position, notes)
	overlay_page_numbers(input_pdf, output_file, 'page_numbers.pdf')


# -----------------
# PROGRAM EXECUTION
# -----------------

# perform example, if run as main module
if __name__ == '__main__':

	input_file = sys.argv[1]
	add_page_numbers(input_file)

