# d_error.py - Unrecoverable error dialog.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later

import traceback
import tkinter as tk
from tkinter import simpledialog

# ErrorDialog-------------------------------------------------------------------
class ErrorDialog(tk.simpledialog.Dialog):

	def __init__(self, parent, error):

		print(traceback.format_exc())
		self.error = str(type(error).__name__) + ': ' + str(error)

		# run parent init
		super().__init__(parent, 'Unrecoverable error')

	def body(self, frame):

		frame.pack(
			fill = tk.BOTH,
			expand = True,
			padx = 50,
			pady = 12
		)

		self.error_label = tk.Label(
			frame,
			text = self.error,
			wraplength = 400,
			justify = tk.LEFT
		)
		self.error_label.pack()

		return frame

	def ok_pressed(self):

		self.destroy()

	def buttonbox(self):

		self.ok_button = tk.Button(self, text='OK', width=5, command=self.ok_pressed)
		self.ok_button.pack(
			side="right",
			padx = 50,
			pady = 12
		)
		self.bind("<Return>", lambda event: self.ok_pressed())

# ErrorDialog-------------------------------------------------------------------

