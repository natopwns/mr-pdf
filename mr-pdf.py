# Mr-PDF.py - A PDF manipulation tool using Python 3.

# Copyright (C) 2019  Nathan Taylor

# Mr-PDF is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Mr-PDF is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Mr-PDF.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import tkinter
import page_numbers
import ui_tkinter
import tkinter_functions as tkf

# Execution---------------------------------------------------------------------
# start gui, if no arguments are given
if len(sys.argv) == 1:

	# error reporting dialog box
	tkinter.Tk.report_callback_exception = tkf.report_callback_exception

	root = tkinter.Tk()
	UserInterface = ui_tkinter.GUI(root)
	root.mainloop()

else:

	for pdf in sys.argv[1:]:
		pdfpath = os.path.abspath(pdf)
		page_numbers.add_page_numbers(pdf)

# Execution---------------------------------------------------------------------

