# Overview

A PDF manipulation tool using Python 3.

# Features

It appends page numbers on the top-right corner of every page of a pdf, with optional notes appended at the bottom-left.
It can detect the orientation of each page individually.

# Limitations

It currently has trouble with non-standard page sizes.

