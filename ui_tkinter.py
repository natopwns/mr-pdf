# ui_tkinter.py - Tkinter GUI formatting instructions.

# This file is part of Mr-PDF.

# Mr-PDF is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Mr-PDF is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Mr-PDF.  If not, see <https://www.gnu.org/licenses/>.


# -------
# IMPORTS
# -------

import tkinter as tk
from tkinter.filedialog import askopenfilename
import page_numbers
import tkinter_functions


# -------
# CLASSES
# -------

class GUI:

	def __init__(self, master):

		# create class-level variable that controls the master tk window
		self.master = master
		# configure the master tk window
		self.master.title("Mr. PDF")
		self.master.resizable(False, False)
		self.master.configure(bg = '#6E232E')
		self.outputText = tk.StringVar()
		self.outputText.set('Ready.')
		self.textColor = 'white'
		self.bgColor = '#6E232E'
		self.buttonColor = '#424242'
		self.aButtonColor = '#515151'
		self.buttonRelief = 'flat'
		# spacer
		self.options = tk.LabelFrame(
			master,
			text = 'Options',
			bg = self.bgColor,
			fg = self.textColor
		)
		self.options.grid(
			padx = 8,
			pady = 6,
			row = 0,
			column = 0
		)
		# number position label
		self.lbl_number = tk.Label(
			self.options,
			text = 'Number position:',
			bg = self.bgColor,
			fg = self.textColor
		)
		self.lbl_number.grid(
			padx = 40,
			pady = 2,
			row = 0,
			column = 0
		)
		# number position radio buttons
		self.position_var = tk.IntVar()
		self.rdo_top = tk.Radiobutton(
			self.options,
			text = 'Top',
			width = '10',
			anchor = 'w',
			variable = self.position_var,
			value = 1,
			bg = self.bgColor,
			fg = self.textColor,
			activebackground = self.bgColor,
			activeforeground = self.textColor,
			selectcolor = self.buttonColor,
			relief = self.buttonRelief
		)
		self.rdo_top.grid(
			padx = 40,
			pady = 2,
			row = 1,
			column = 0
		)
		self.rdo_bottom = tk.Radiobutton(
			self.options,
			text = 'Bottom',
			width = '10',
			anchor = 'w',
			variable = self.position_var,
			value = 2,
			bg = self.bgColor,
			fg = self.textColor,
			activebackground = self.bgColor,
			activeforeground = self.textColor,
			selectcolor = self.buttonColor,
			relief = self.buttonRelief
		)
		self.rdo_bottom.grid(
			padx = 40,
			pady = 2,
			row = 2,
			column = 0
		)
		# spacer
		self.spacer = tk.LabelFrame(
			self.options,
			bg = self.bgColor
		)
		self.spacer.grid(
			padx = 40,
			pady = 4,
			row = 3,
			column = 0
		)
		# notes label
		self.lblNotes = tk.Label(
			self.options,
			text = 'Notes:',
			bg = self.bgColor,
			fg = self.textColor
		)
		self.lblNotes.grid(
			padx = 40,
			pady = 2,
			row = 4,
			column = 0
		)
		# notes entry box
		self.txtNotes = tk.Entry(
			self.options,
			relief = 'flat',
			bg = '#424242',
			fg = 'white'
		)
		self.txtNotes.grid(
			padx = 30,
			pady = 4,
			row = 5,
			column = 0
		)
		# spacer
		self.spacer2 = tk.LabelFrame(
			self.options,
			bg = self.bgColor
		)
		self.spacer2.grid(
			padx = 40,
			pady = 4,
			row = 6,
			column = 0
		)
		# add numbers button
		self.btnOrganize = tk.Button(
			master,
			text = 'Add Numbers to PDF',
			width = 18,
			command = self.ui_add_page_numbers,
			bg = self.buttonColor,
			fg = self.textColor,
			activebackground = self.aButtonColor,
			activeforeground = self.textColor,
			relief = self.buttonRelief
		)
		self.btnOrganize.grid(
			padx = 40,
			pady = 6,
			row = 1,
			column = 0
		)
		# feedback label
		self.lblOutput = tk.Label(
			master,
			textvariable = self.outputText,
			bg = self.bgColor,
			fg = self.textColor
		)
		self.lblOutput.grid(
			padx = 50,
			pady = 12,
			row = 2,
			column = 0
		)

		# default options
		self.rdo_top.select()

	def ui_add_page_numbers(self):

		inputFile = tkinter_functions.tk_pick_file(self)

		if inputFile == False:
			return

		page_numbers.add_page_numbers(inputFile, self.position_var.get(), self.txtNotes.get())

		self.outputText.set('Done.')

